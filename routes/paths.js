var express = require('express');
var router = express.Router();
var _ = require('lodash');

var Path = require('../models/path');

//build the REST operations at the base for paths
//this will be accessible from http://localhost:8080/paths if the default route for / is left unchanged
router.route('/')
//GET all paths
    .get(function (req, res, next) {
        res.json(Path.getAll());
    })
    //POST a new path
    .post(function (req, res, next) {
        var path = Path.create();
        res.json(path);
    });

router.route('/:pathID')
    .put(function (req, res, next) {
        var path = Path.getByID(req.params.pathID);

        // Update object
        _.merge(path, req.body);

        // merge fail for bookins, dont know why
        path.bookings = req.body.bookings;

        res.json(path);
    });

module.exports = router;
