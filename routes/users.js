var express = require('express');
var router = express.Router();
var _ = require('lodash');

var User = require('../models/user');

//build the REST operations at the base for paths
//this will be accessible from http://localhost:8080/paths if the default route for / is left unchanged
router.route('/')
//GET all users
    .get(function (req, res, next) {
        res.json(User.getAll());
    });

router.route('/:userID')
    .get(function (req, res, next) {
        var user = User.getByID(req.params.userID);

        res.json(user);
    })
    .put(function (req, res, next) {
        var user = User.getByID(req.params.userID);

        // if user exist we update
        if (user) {
            // Update object
            _.merge(user, req.body);

            // merge fail for bookins, dont know why
            user.childrens = req.body.childrens;
        } else {
            // if user dont exist we create
            user = User.create(req.body._id, req.body.login, req.body.password, req.body.firstname, req.body.lastname);
        }

        res.json(user);
    });

module.exports = router;
