var crypto = require('crypto');
var _ = require('lodash');


// fake data
var users = [
    {
        _id: crypto.randomBytes(20).toString('hex'),
        login: "Peticlette",
        password: "peticlette",
        firstname: "John",
        lastname: "Doe",
        childrens: [
            {
                _id: crypto.randomBytes(20).toString('hex'),
                firstname: "Albert",
                lastname: "Doe",
                birthdate: "01/12/2014"

            },
            {
                _id: crypto.randomBytes(20).toString('hex'),
                firstname: "José",
                lastname: "Doe",
                birthdate: "06/23/2011"
            },
            {
                _id: crypto.randomBytes(20).toString('hex'),
                firstname: "René",
                lastname: "Doe",
                birthdate: "01/11/2009"
            }
        ]
    }
];


exports.create = function (id, login, password, firstname, lastname) {
    var user = {
        _id: id,
        login: login,
        password: password,
        firstname: firstname,
        lastname: lastname,
        childrens: []
    };

    users.push(user);

    return user;
};

exports.update = function (id, newUser) {
    users[id] = newUser;
};

exports.getAll = function () {
    return users;
};

exports.getByID = function (id) {
    return _.filter(users, {_id: id})[0];
};