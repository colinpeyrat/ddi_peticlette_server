var crypto = require('crypto');
var _ = require('lodash');


var today = new Date();

// fake data
var paths = [
    {
        _id: "161fdf92f86b599806541cc2d05cfabf1098f85c",
        date: today.setDate(today.getDate() + 1),
        start: {
            adress: "MJC des Carrés",
            pos: {
                lat: 45.915197,
                lng: 6.146152
            }
        },
        end: {
            adress: "Ecole & College La Salle",
            pos: {
                lat: 45.917904,
                lng: 6.149379
            }
        },
        companion: "Paul",
        bookings: [],
        bookingsLimit: 3
    },
    {
        _id: "7e9d18e871cf19d1a425377360943c9aabfc82d2",
        date: today.setDate(new Date().getDate() + 6),
        start: {
            adress: "La Turbine",
            pos: {
                lat: 45.906465,
                lng: 6.107410
            }
        },
        end: {
            adress: "Ecole Primaire Sous Aléry",
            pos: {
                lat: 45.899210,
                lng: 6.113553
            }
        },
        companion: "Arnaud",
        bookings: [],
        bookingsLimit: 8
    },
    {
        _id: "e26cad46e5abac00f60a9c800acbeab0c7298a1e",
        date: today.setDate(new Date().getDate()),
        start: {
            adress: "Le Petit Port",
            pos: {
                lat: 45.905504,
                lng: 6.158128
            }
        },
        end: {
            adress: "Les Tilleuls",
            pos: {
                lat: 45.911192,
                lng: 6.137105
            }
        },
        companion: "Thomas",
        bookings: [],
        bookingsLimit: 5
    },
    {
        _id: "efb8adb6a4a1ac555ca64c2a59a8fbe3be4e61b9",
        date: today.setDate(new Date().getDate() + 13),
        start: {
            adress: "Mairie Annecy le vieux",
            pos: {
                lat: 45.919297,
                lng: 6.143171
            }
        },
        end: {
            adress: "Ecole des Glaisins",
            pos: {
                lat: 45.922926,
                lng: 6.159279
            }
        },
        companion: "Alice",
        bookings: [],
        bookingsLimit: 12
    }
];


exports.create = function (coordinate, date, companion, booking) {
    var path = {
        _id: crypto.randomBytes(20).toString('hex'),
        coordinate: coordinate,
        date: date,
        companion: companion,
        booking: booking
    };

    paths.push(path);

    return path;
};

exports.update = function (id, newPath) {
    paths[index] = newPath;
};

exports.getAll = function () {
    return paths;
};

exports.getByID = function (id) {
    return _.filter(paths, {_id: id})[0];
};