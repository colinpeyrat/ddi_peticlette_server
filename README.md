#Peticlette API

L'api de l'application **Peticlette** ([sources](https://bitbucket.org/colinpeyrat/ddi_peticlette) de l'application )

##Installation
```
$ npm install
$ npm start
```

## Utilisation

### Exemple

`http://localhost:8080/paths` pour obtenir tous les trajets

`http://localhost:8080/paths/id` pour obtenir un trajet

`http://localhost:8080/users` pour obtenir tous les utilisateurs

etc.